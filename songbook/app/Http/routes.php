<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Http\Controllers;


Route::get('/', 'ArtistController@getAllArtists');

Route::get('/artist/{artistAlias}', 'ArtistController@getArtistByAlias');

Route::get('/genre/{genreAlias}', 'ArtistController@getArtistsByGenre');

Route::get('/song/{artistAlias}/{songAlias}', 'SongController@getArtistSongByAlias');
