<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Genre;
use Illuminate\Routing\Controller as BaseController;

class ArtistController extends BaseController {

    public function getAllArtists()
    {
        $artists = Artist::get();


        return view('artists', ['artists' => $artists]);
    }

    public function getArtistByAlias($artistAlias)
    {
        $artist = Artist::where('alias', $artistAlias)->first();

        if ($artist) {
            return view('artist', ['artist' => $artist]);
        }

        return redirect('/');
    }

    public function getArtistsByGenre($genreAlias)
    {
        $genre = Genre::where('alias', $genreAlias)->first();
        if ($genre) {
            $artists = $genre->artists;
            return view('artists', ['artists' => $artists]);
        }

        return redirect('/');
    }

}
