<?php

namespace App\Http\Controllers;

use App\Artist;
use Illuminate\Routing\Controller as BaseController;

class SongController extends BaseController {

    public function getArtistSongByAlias($artistAlias, $songAlias)
    {
        $artist = Artist::where('alias', $artistAlias)->first();

        if ($artist) {
            $song = $artist->songs->where('alias', $songAlias)->first();
            if ($song) {
                return view('song', ['song' => $song, 'artist' => $artist]);
            }
        }

        return redirect('/');
    }

}
