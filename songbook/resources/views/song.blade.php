@extends('layouts.app')

@section('content')

    @if ($song)
        <div class="card card-cascade wider">
            <div class="card-block text-center">
                <h4 class="card-title">
                    <strong>{{$artist->name}} - {{$song->name}}</strong>
                </h4>
                <h5>
                    <strong>Song text</strong>
                </h5>
                <div class="card-text">
                   <div class="bottom-buffer">{!! nl2br($song->text) !!} </div>
                   <div class="genres bottom-buffer">
                        <strong>Genres:</strong>
                        @if (count($artist->genres) > 0)
                            @foreach ($artist->genres as $genre)
                                <a href="/genre/{{$genre->alias}}">{{$genre->name}}</a>
                            @endforeach
                        @endif
                   </div>
                </div>
                <a href="/" class="btn btn-primary">Go back</a>
            </div>
        </div>
    @endif

@endsection
