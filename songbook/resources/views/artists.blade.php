@extends('layouts.app')

@section('content')

    @if (count($artists) > 0)
        <div class="container">
            <div class="row">
                @foreach ($artists as $artist)
                    <div class="col-md-4 col-sm-6 col-12 bottom-buffer">
                        <div class="card card-cascade wider">

                            <!--Card image-->
                            <img src="/public/img/{{$artist->photo}}" class="card-img-top img-fluid">
                            <!--/Card image-->

                            <div class="card-block text-center">
                                <h4 class="card-title"><strong>{{$artist->name}}</strong></h4>
                                <h5>Количество песен в каталоге: {{count($artist->songs)}}</h5>
                                <p class="genres card-text">
                                    Genres:
                                    @if (count($artist->genres) > 0)
                                        @foreach ($artist->genres as $genres)
                                            <a href="/genre/{{$genres->alias}}">{{$genres->name}}</a>
                                        @endforeach
                                    @endif
                                </p>
                                <a href="/artist/{{$artist->alias}}" class="btn btn-primary">Artist page</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endsection
