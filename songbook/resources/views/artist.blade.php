@extends('layouts.app')

@section('content')

    @if ($artist)
        <div class="no-border card card-cascade wider">
            <!--Card image-->
            <img src="/public/img/{{$artist->photo}}" class="img-fluid">
            <!--/Card image-->

            <div class="card-block text-center">
                <h4 class="card-title">
                    <strong>{{$artist->name}}</strong>
                </h4>
                <h5>
                    <strong>Biography</strong>
                </h5>
                <div class="card-text">
                   <div class="bottom-buffer">{{$artist->biography}}</div>
                    <div class="songs">
                        <strong>Songs:</strong>
                        @if (count($artist->songs) > 0)
                            @foreach ($artist->songs as $song)
                                <a href="/song/{{$artist->alias}}/{{$song->alias}}">{{$song->name}}</a>
                            @endforeach
                        @endif
                   </div>
                   <div class="genres bottom-buffer">
                        <strong>Genres:</strong>
                        @if (count($artist->genres) > 0)
                            @foreach ($artist->genres as $genre)
                                <a href="/genre/{{$genre->alias}}">{{$genre->name}}</a>
                            @endforeach
                        @endif
                   </div>
                </div>
                <a href="/" class="btn btn-primary">Go back</a>
            </div>
        </div>
    @endif

@endsection
