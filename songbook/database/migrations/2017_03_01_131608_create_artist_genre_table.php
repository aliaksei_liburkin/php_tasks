<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistGenreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_genre', function (Blueprint $table)
        {
            $table->integer('artist_id')->unsigned()->nullable();
            $table->foreign('artist_id')->references('id')
                ->on('artists')->onDelete('cascade');
            $table->integer('genre_id')->unsigned()->nullable();;
            $table->foreign('genre_id')->references('id')
                ->on('genres')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artist_genre');
    }
}
