<?php

class Figure
{
    public $area;
    public $color;

    public function infoAbout()
    {
        return 'Это геометрическая фигура!';
    }

}

class Rectangle
{
    private $a;
    private $b;

    const SIDES_COUNT = 4;

    public function __construct($a, $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    public function getArea()
    {
        return $this->a * $this->b;
    }

    final public function infoAbout()
    {
        return 'Это класс прямоугольника. У него ' . self::SIDES_COUNT . ' стороны';
    }

}

class Triangle
{
    private $a;
    private $b;
    private $c;

    const SIDES_COUNT = 3;

    public function __construct($a, $b, $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    public function getArea()
    {   $p = ($this->a + $this->b + $this->c) / 2;
        return sqrt($p * ($p - $this->a) * ($p - $this->b) * ($p - $this->c));
    }

    final public function infoAbout()
    {
        return 'Это класс треугольника. У него ' . self::SIDES_COUNT . ' стороны';
    }

}

class Square
{
    private $a;

    const SIDES_COUNT = 4;

    public function __construct($a)
    {
        $this->a = $a;
    }

    public function getArea()
    {
        return $this->a * $this->a;
    }

    final public function infoAbout()
    {
        return 'Это класс квадрата. У него ' . self::SIDES_COUNT . ' стороны';
    }

}

$square1 = new Square(12);
$square2 = new Square(6);
$triangle1 = new Triangle(6, 6, 6);
$triangle2 = new Triangle(12, 6, 8);
$rectangle1 = new Rectangle(12, 6);
$rectangle2 = new Rectangle(4, 3);

echo '<pre>';
echo $square1->getArea() . PHP_EOL;
echo $square2->getArea() . PHP_EOL;
echo $triangle1->getArea() . PHP_EOL;
echo $triangle2->getArea() . PHP_EOL;
echo $rectangle1->getArea() . PHP_EOL;
echo $rectangle2->getArea() . PHP_EOL;
echo '</pre>';


//$class = "Square";
//$object = new $class(12);
//
//print_r($object);

?>
