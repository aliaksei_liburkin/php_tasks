<?php

class Car
{
    public $name;
    public $price;
    public $color;
    public $year;
    public $fuel;
    public $numberOfDoors = 4;


    const FIRST_CONST = 2;
    const SECOND_CONST = 5;
    const THIRD_CONST = 12;

    function __construct()
    {
        $args = func_get_args();
        $args_count = func_num_args();
        if (method_exists($this, $f = '__construct' . $args_count)) {
            call_user_func_array(array($this, $f), $args);
        }
    }

    public function __construct5($name, $price, $color, $year, $fuel)
    {
        $this->name = $name;
        $this->price = $price;
        $this->color = $color;
        $this->year = $year;
        $this->fuel = $fuel;
    }

    public function fuelСonsumption($distance)
    {
        return $this->fuel/100 * $distance;
    }

    static public function getMaxConstant()
    {
       return max(self::FIRST_CONST, self::SECOND_CONST, self::THIRD_CONST);
    }

}

$car1 = new Car();
$car1->name = 'Honda Legend';
$car1->price = 7300;
$car1->color = 'black';
$car1->year = 2006;
$car1->fuel = 17.0;

$car2 = new Car();
$car2->name = 'Honda Civic';
$car2->price = 2900 ;
$car2->color = 'green';
$car2->year = 1997;
$car2->fuel = 10.2;

$car3 = new Car('Toyota Yaris', 4900, 'white', 2007, 6.4);
$car4 = new Car('Lincoln Navigator', 7000, 'black', 1998, 18.1);

echo '<pre>';
echo 'Fuel consumption: ' . $car3->fuelСonsumption(1000) . 'l' . PHP_EOL;
echo 'Constants: ' . Car::FIRST_CONST . ' ' . Car::SECOND_CONST . ' ' . Car::THIRD_CONST . PHP_EOL;
echo 'Max const: ' . Car::getMaxConstant() . PHP_EOL;
echo '</pre>';


?>
