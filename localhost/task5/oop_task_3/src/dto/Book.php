<?php

class Book
{
    private $id;
    private $name;
    private $author;
    private $path;

    public function __construct($id, $name, $author, $path)
    {
        $this->id = $id;
        $this->name = $name;
        $this->author = $author;
        $this->path = $path;
    }

    public function setId($id) {
      $this->id = $id;
    }

    public function getId() {
      return $this->id;
    }

    public function setName($name) {
      $this->name = $name;
    }

    public function getName() {
      return $this->name;
    }

    public function setAuthor($author) {
      $this->author = $author;
    }

    public function getAuthor() {
      return $this->author;
    }

    public function setPath($path) {
      $this->path = $path;
    }

    public function getPath() {
      return $this->path;
    }
}

?>
