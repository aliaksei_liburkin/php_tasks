<?php

include_once('IBookDAO.php');
include_once('./db/Database.php');
include_once('./factory/BookFactory.php');
include_once('./dto/BookPdf.php');
include_once('./dto/BookTxt.php');
include_once('./dto/BookDoc.php');





class MySQLBookDAO implements IBookDAO
{
	public function getAllBooks()
    {

        $db = Database::getInstance();
        $connection = $db->getConnection();
        $query = 'SELECT * FROM books';
        $result = $connection->query($query);

        $books = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $bookClassName = BookFactory::getBookClassName($row['type']);
                $book = new $bookClassName($row['id'], $row['name'], $row['author'], $row['path']);
                array_push($books, $book);
            }
        }

        return $books;

    }
}

?>
