<?php

include_once('./factory/DAOFactory.php');

$bookDAO = DAOFactory::getFactory()->getBookDAO();
$books = $bookDAO->getAllBooks();

foreach ($books as $book) {
    echo '<p style="display: flex">
            <img width="50" height="50" src="' . $book::IMAGE_PATH . '" alt="" />
            <a style="margin: inherit;padding-left:10px;" href="' . $book->getPath() . '">' . $book->getAuthor() . ', ' . $book->getName() . '</a>
          </p>';
}

?>
