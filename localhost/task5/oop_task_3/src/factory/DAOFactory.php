<?php

include_once('./dao/MySQLBookDAO.php');

class DAOFactory
{
	private static $instance;

	public function __construct()
	{
	}

	public static function setFactory(DAOFactory $f)
	{
		self::$instance = $f;
	}

	public static function getFactory()
	{
		if(!self::$instance)
			self::$instance = new self;

		return self::$instance;
	}

	public function getBookDao()
	{
		return new MySQLBookDAO();
	}
}

?>
