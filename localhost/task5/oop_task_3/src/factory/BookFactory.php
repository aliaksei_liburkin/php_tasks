<?php

class BookFactory
{

    private static $PDF = 'pdf';
    private static $TXT = 'txt';
    private static $DOC = 'doc';

    public static function getBookClassName($bookType)
    {
        switch ($bookType) {
            case self::$PDF:
                return 'BookPdf';
                break;
            case self::$DOC:
                return 'BookDoc';
                break;
            case self::$TXT:
                return 'BookTxt';
                break;
        }
    }
}
?>
