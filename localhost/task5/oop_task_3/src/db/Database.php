<?php

class Database {
	private $connection;
	private static $instance;
	private $host = "localhost";
	private $username = "root";
	private $password = "";
	private $database = "task_5_DB";

    private function __construct()
    {
		$this->connection = new mysqli($this->host, $this->username,
			$this->password, $this->database);

		if ($this->connection->connect_error) {
            trigger_error("Failed to connect to to MySQL: " . $this->connection->connect_error, E_USER_ERROR);
		}
	}

	public static function getInstance()
    {
		if (!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __clone() { }

	public function getConnection()
    {
		return $this->connection;
	}
}
?>
