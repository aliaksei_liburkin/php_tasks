<?php

function printResult($result)
{
    echo '<pre>';
    echo $result;
    echo '</pre>';
}

setcookie('welcom', ' ', time() + 36000);

setcookie('lastDate', time(), time() + 36000);

if (isset($_COOKIE['count'])) {
    $count = $_COOKIE['count'] + 1;
} else {
    $count = 0;
}

setcookie('count', $count, time() + 36000);

if (!isset($_COOKIE['welcom'])) {
    printResult("Добро пожаловать, новичек!");
} else {
    printResult("С возвращением, дружище!");
}

if (isset($_COOKIE['lastDate'])) {
    printResult(date('l jS \of F Y h:i:s A', $_COOKIE['lastDate']));
}

if (isset($_COOKIE['count'])) {
    printResult('Это ваше <strong>' . $_COOKIE['count'] . '</strong> посещение');
}

?>
