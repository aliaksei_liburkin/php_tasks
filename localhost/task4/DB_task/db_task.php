<?php

$host='localhost';
$database='aliaksei_liburkin_DB';
$user='root';
$pswd='';

$connect = mysqli_connect($host, $user, $pswd,$database);

if (!$connect) {
    echo "<pre>";
    printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error());
    echo "</pre>";
    exit;
} else {
    echo "<pre>";
    echo "Соединение установленно!";
    echo "</pre>";
}


$query = "INSERT INTO users (id, first_name, last_name) VALUES
          (NULL, 'Александр','Петров'),
          (NULL, 'Джони', 'Блэк'),
          (NULL, 'Джони', 'Вайт'),
          (NULL, 'Петр', 'Петров'),
          (NULL, 'Василий', 'Сидоров')";

if (mysqli_query($connect, $query)) {
    echo "<pre>";
    echo "Записи добавлены";
    echo "</pre>";
} else {
    echo "<pre>";
    echo "Ошибка: " . $query . "<br>" . mysqli_error($connect);
    echo "</pre>";
}


$query = "DELETE FROM users WHERE users.first_name = 'Томас';
          DELETE FROM users WHERE users.first_name = 'Томас' and users.last_name = 'Смит';
          DELETE FROM users WHERE users.first_name = 'Томас' or users.first_name = 'Джон';";

if (mysqli_multi_query($connect, $query)) {
    echo "<pre>";
    echo "Записи удалены";
    echo "</pre>";
} else {
    echo "<pre>";
    echo "Ошибка: " . $query . "<br>" . mysqli_error($connect);
    echo "</pre>";
}

while (mysqli_next_result($connect)) {;}

$query = "SELECT id FROM users ORDER BY id DESC LIMIT 3";
$result = mysqli_query($connect, $query);

$query = "UPDATE users SET first_name = 'Тимофей', last_name = 'Опель' WHERE users.id = 3; ";

if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $userID = $row['id'];
        $query .= "UPDATE users SET last_name = 'Смит' WHERE users.id = $userID; ";
    }
} else {
    echo "<pre>";
    echo "Ничего не найденно";
    echo "</pre>";

}

if (mysqli_multi_query($connect, $query)) {
    echo "<pre>";
    echo "Записи обновлены";
    echo "</pre>";
} else {
    echo "<pre>";
    echo "Ошибка: " . $query . "<br>" . mysqli_error($connect);
    echo "</pre>";
}

while (mysqli_next_result($connect)) {;}


echo "3 последних новости из категории с идентификатором 3: ";

$query = "SELECT * FROM news WHERE category_id = 3 ORDER BY id DESC LIMIT 3";
$result = mysqli_query($connect, $query);

if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        echo "<pre>";
        print_r($row);
        echo "</pre>";
    }
} else {
    echo "<pre>";
    echo "Ничего не найденно.";
    echo "</pre>";
}

$query = "SELECT * FROM users WHERE first_name = 'Владислав' or first_name = 'Елена'";
$result = mysqli_query($connect, $query);

echo "ПОЛЬЗОВАТЕЛИ С ИМЕНЕМ ВЛАДИСЛАВ И ЕЛЕНА: ";

if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        echo "<pre>";
        print_r($row);
        echo "</pre>";
    }
} else {
    echo "<pre>";
    echo "Ничего не найденно.";
    echo "</pre>";
}

$query = "SELECT COUNT(*) as count FROM news WHERE category_id = 0 or category_id = 1";
$result = mysqli_query($connect, $query);

echo "Количество новостей со статусом \"1\" и \"0\": ";

if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        echo "<pre>";
        echo $row['count'];
        echo "</pre>";
    }
} else {
    echo "<pre>";
    echo "Ничего не найденно.";
    echo "</pre>";
}


echo "Все новости со статусом \"1\" в виде списка: ";

$query = "SELECT * FROM news WHERE category_id = 1";
$result = mysqli_query($connect, $query);

if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {

        $title = $row['title'];
        $description = $row['description'];
        $id = $row['id'];

        echo "<h1> $title </h1>
              <p> $description </p>
              <a href='/news/view/$id'>Читать далее </a>";
    }
} else {
    echo "<pre>";
    echo "Ничего не найдено.";
    echo "</pre>";
}

?>
