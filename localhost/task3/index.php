<?php
    
//Либуркин Алексей
/*
 * 25.01.2017
 */

$newLine = '</br>';


function printResult($result)
{
    echo '<pre>';
    print_r($result);
    echo '</pre>';
}


echo "LESSON 6 $newLine TASK 1";


$products = array(
    array('name' => 'Телевизор', 'price' => '400', 'quantity' => 1),
    array('name' => 'Телефон', 'price' => '300', 'quantity' => 3),
    array('name' => 'Кроссовки', 'price' => '150', 'quantity' => 2),
);


function getTotalInfo($products)
{
    foreach ($products as $product) {
        $totalPrice += $product['price'];
        $totalQuantity += $product['quantity'];
    }
    return array('totalPrice' => $totalPrice, 'totalQuantity' => $totalQuantity);
}


printResult(getTotalInfo($products));

echo 'TASK 2';


function quadraticSolver($a, $b, $c)
{
    if ($a != 0) {
        $d = ($b * $b) - (4 * $a * $c);
        if ($d > 0) {
            $x1 = (-$b + sqrt($d)) / (2 * $a);
            $x2 = (-$b - sqrt($d)) / (2 * $a);
            return array($x1, $x2);
        } elseif ($d == 0) {
            $x = -$b / (2 * $a);
            return $x;
        } else {
            return false;
        }
    }
}

printResult(quadraticSolver(1, 1, -6));

echo 'TASK 3';


function deleteNegativesV1($digits)
{
    foreach ($digits as $id => $digit) {
        if ($digit < 0) {
            unset($digits[$id]);
        }
    }
    return $digits;
}


$digits = array(2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95);
printResult(array_values(deleteNegativesV1($digits)));

echo 'TASK 4';


function deleteNegativesV2(&$digits)
{
    foreach ($digits as $id => $digit) {
        if ($digit < 0) {
            unset($digits[$id]);
        }
    }
}


$digits = array(2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95);
deleteNegativesV2($digits);
printResult(array_values($digits));

echo "________________________ $newLine $newLine";

echo "LESSON 8 $newLine $newLine TASK 1";

$result = $_POST['int'];


if (isset($result)) {
    printResult('max = ' . max(toInteger($result)));
    printResult('min = ' . min(toInteger($result)));
    printResult('avg = ' . avg(toInteger($result)));
} else {
    printResult('Форма не отправленна');
}


function toInteger($numbers)
{
    foreach ($numbers as $id => $number) {
        $numbers[$id] = intval($number);
    }
    return $numbers;
}


function avg($array)
{
    if (! empty($array)) {
       return array_sum($array) / count($array);
    }
}


echo 'TASK 2';

$name = $_POST['name'];
$gender = $_POST['gender'];


if (isset($name) && isset($gender)) {
    switch ($gender) {
        case 'male':
            printResult("Добро пожаловать, мистер $name !");
            break;
        case 'female':
            printResult("Добро пожаловать, миссис $name !");
            break;
        default:
            printResult('Неверный пол');
    }
} else {
    printResult('Форма не отправленна');
}


?>

<h2>TASK 1</h2>
<div>
    <form action="index.php" method="POST">
        <p><input type="text" name="int[]" /></p>
        <p><input type="text" name="int[]"/></p>
        <p><input type="text" name="int[]"/></p>
        <p><input type="text" name="int[]"/></p>
        <p><input type="text" name="int[]"/></p>
        <p><input type="text" name="int[]"/></p>
        <p><input type="text" name="int[]"/></p>
        <br/>
        <input type="submit" name="submit" value="submit" />
    </form>
</div>
<h2>TASK 2</h2>
<div>
    <form action="index.php" method="POST">
        <p>name: <input type="text" name="name" /></p>
        <p>female <input type="radio" name="gender" value="female"/></p>
        <p>male <input type="radio" name="gender" value="male"/></p>
        <br/>
        <input type="submit" name="submit" value="submit" />
    </form>
</div>
