<?php
    //Либуркин Алексей
    /*
     * 10.01.2017
     */
    
    $newLine = "</br>";

    //task 1 
    $a = 152;
    $b = '152';
    $c = 'London';
    $d = array(152);
    $e = 15.2;
    $f = false;
    $g = true;

    echo gettype($a) . $newLine .
         gettype($b) . $newLine .
         gettype($c) . $newLine .
         gettype($d) . $newLine .
         gettype($e) . $newLine .
         gettype($f) . $newLine .
         gettype($g) . $newLine . $newLine;

    //task 2
    $a = 19;
    $b = 20;

    echo "{$a} из {$b}ти человек сдало экзамен $newLine";
    echo  $a . ' из ' . $b . 'ти человек сдало экзамен' . $newLine . $newLine ;

    //task 3
    $str1 = 'Доброе утро';
    $str2 = 'дамы';
    $str3 = 'и господа';

    echo $str1 . $newLine;
    echo $str2 . $newLine;
    echo $str3 . $newLine;
    echo $str1 . ', ' . $str2 . ' ' . $str3 . '.' . $newLine . $newLine;    


    //task 4
    $firstArray = array(1, 2, 3, 4, 5);
    $secondArray = array("One", "Two", "Three", "Four", "Five");
    $firstArray["element"] = "value";
    unset($secondArray[0]);

    echo "Second elements: $firstArray[2], $secondArray[2] $newLine";

    echo "First array: ";
    print_r($firstArray);
    echo $newLine;

    echo "Second array: ";
    print_r($secondArray);
    echo $newLine;

    echo 'First array count: ' . count($firstArray) . $newLine;
    echo 'First array count: ' . count($secondArray) . $newLine . $newLine;


    //task 5
    define("MIN", 10);
    define("MAX", 50);
    $x = 40;

    if ($x > MIN && $x < MAX) {
        echo "+";
    } elseif ($x == MIN || $x == MAX) {
        echo "+-";
    } else {
        echo "-";
    }

    echo $newLine . $newLine;


    //task 6
    $a = 1;
    $b = 1;
    $c = -6;

    $d = $b * $b -4 * $a * $c;

    if ($d > 0) {
        $x1 = (-$b + sqrt($d)) / (2 * $a);
        $x2 = (-$b - sqrt($d)) / (2 * $a);
        echo "Первый корень: $x1, Второй корень: $x2";
    } elseif($d == 0) {
        $x = -$b / (2 * $a);
        echo "Корень $x";
    } else {
        echo "Нет корней";
    }

    echo $newLine . $newLine;


    //task 7 
    if ($a != $b && $b != $c && $a != $c) {
        echo ($a + $b + $c) / 3;
    } else {
        echo "Ошибка";
    }

    echo $newLine . $newLine;


    //task 8
    $y   = 1;
    $sum = 0;

    while ($y < 25) {
        $sum += $y;
        $y++;
    }

    echo "while - $sum $newLine";

    $sum = 0;

    for ($y = 1;$y < 25;$y++) {
        $sum += $y;
    }

    echo "for - $sum $newLine $newLine";

    //task 9
    $n      = 20;
    $y      = 1;
    $result = 0;
    while ($y <= $n) {
        $result = $y * $y;
        echo "Квадрат числа $y = $result $newLine";
        $y++;
    }

    echo $newLine;


    //task 10
    $menu = array("Кнопка 10", "Кнопка 9", "Кнопка 8", "Кнопка 7", "Кнопка 6", "Кнопка 5", "Кнопка 4", "Кнопка 3", "Кнопка 2", "Кнопка 1");
    $menu = array_reverse($menu);
    echo '<ul>';

    foreach ($menu as $v) {
        echo "<li><a href='#'>$v</a></li>";
    }

    echo '</ul>';

?>
